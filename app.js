/**
 * Rock, Paper, Scissors Game made by Mishal Alajmi
 * gitlab: https://gitlab.com/zerotalent/rock-paper-scissors.git
 * 04/28/2019
 */

 // Define all possible plays
const playMove = ['Rock','Paper','Scissors'];

let playerScore = 0;
let compScore = 0;
let roundCount = 0;

// Cache DOM elements
const playerScore_span = document.getElementById('player-score');
const compScore_span = document.getElementById('comp-score');
const round_span = document.getElementById('round-count');

// Player rps images
const rock_img = document.getElementById('rock-hand');
const paper_img = document.getElementById('paper-hand');
const scissors_img = document.getElementById('scissors-hand');

// Computer rps images
const comp_rock_img = document.getElementById('comp-rock-hand');
const comp_paper_img = document.getElementById('comp-paper-hand');
const comp_scissors_img = document.getElementById('comp-scissors-hand');

// Banner
const banner_p = document.getElementById('banner');

    
/**
 * @params none
 * @return a random value from 0..2
 */    
function computerPlay() {
    let compMove = Math.floor(Math.random() * 3);

    console.log(`Comp chose ${playMove[compMove]}`);
    return playMove[compMove]; 
}

/**
 * @params none
 * @return Gets a players move and returns it
 */
function playerPlay() {
    // Determine which move the player made
    rock_img.onclick = () => {
        console.log('rock clicked');
        game(playMove[0]);
    }
    paper_img.onclick = () => {
        console.log('paper clicked');
        game(playMove[1]);
    }
    scissors_img.onclick = function() {
        console.log('scissors clicked');
        game(playMove[2]);
    }

}


function playerWin(playerMove,ComputerMove) {
    playerScore += 1;
    playerScore_span.innerText = playerScore;

    banner_p.innerText = "Player Wins";    
}

function playerLose(playerMove,computerMove) {
    compScore += 1;
    compScore_span.innerText = compScore;

    banner_p.innerText = "Computer Wins";
}

function draw() {
    banner_p.innerText = "Draw";
}
/**
 * @params ComputerMove, PlayerMove
 * @output Returns the conclusion of the game.
 */
function game(playerMove) {

    let computerMove = computerPlay();

    if (playerMove === computerMove) {
        draw();
    }

    if (playerMove === 'Rock' && computerMove === 'Scissors') {
        console.log('player wins')
        playerWin();

    } 
    if (playerMove === 'Paper' && computerMove === 'Rock') {
        console.log('player wins')
        playerWin();

    } 
    if (playerMove === 'Scissors' && computerMove === 'Paper') {
        console.log('player wins')
        playerWin();

    }
    if (playerMove === 'Rock' && computerMove === 'Paper') {
        console.log('player loses')
        playerLose();

    }
    if (playerMove === 'Paper' && computerMove === 'Scissors') {
        console.log('player loses')
        playerLose();

    }
    if (playerMove === 'Scissors' && computerMove === 'Rock') {
        console.log('player loses')
        playerLose();
    }
    // increase the round count
    roundCount += 1;
    round_span.innerHTML = roundCount;
}

// Wait for the player's move
playerPlay();

